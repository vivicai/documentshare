if (window.addEventListener)
	window.addEventListener('DOMMouseScroll', handleWheel, false);
window.onmousewheel = document.onmousewheel = handleWheel;

if (window.attachEvent)
	window.attachEvent("onmousewheel", handleWheel);

function handleWheel(event) {
	try {
		window.document.docViewer_1_swf.setViewerFocus(true);
		window.document.docViewer_1_swf.focus();

		if (navigator.appName == "Netscape") {
			if (event.detail)
				delta = 0;
			if (event.preventDefault) {
				event.preventDefault();
				event.returnValue = false;
			}
		}
		return false;
	} catch (err) {
		return true;
	}
}