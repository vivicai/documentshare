<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.core.ado.IDataObjectQuery"%>
<%@ page import="net.simpleframework.content.news.NewsUtils"%>
<%@ page import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@ page import="net.simpleframework.content.news.News"%>
<%@ page import="net.simpleframework.util.ConvertUtils"%>
<%@ page import="net.simpleframework.content.component.newspager.INewsPagerHandle"%>
<%@ page import="net.simpleframework.content.news.INewsApplicationModule"%>
<%@ page import="net.simpleframework.content.ContentLayoutUtils"%>
<%
	final ComponentParameter nComponentParameter = new ComponentParameter(
			request, response, null);
	final IDataObjectQuery<?> qs = ContentLayoutUtils
			.getQueryByRequest(nComponentParameter);
	if (qs == null) {
		return;
	}
	nComponentParameter.componentBean = NewsUtils.applicationModule
			.getComponentBean(nComponentParameter);
	if (nComponentParameter.componentBean == null) {
%>
<div style="text-align: center; padding: 8px;">#(news_layout.0)</div>
<%
	return;
	}
	final INewsPagerHandle nHandle = (INewsPagerHandle) nComponentParameter
			.getComponentHandle();
%>
<div class="list_layout">
<%
	News news;
	int i = 0;
	while ((news = (News) qs.next()) != null) {
		if (ContentLayoutUtils.isRequestNews(nComponentParameter, qs,
				news)) {
			continue;
		}
		final boolean isNews = nHandle.isRemarkNew(nComponentParameter,
				news);
%>
<div class="rrow"
	style="background: url('<%=ContentLayoutUtils.dotImagePath(nComponentParameter,
						NewsUtils.deployPath)%>') 5px 5px no-repeat;">
<table style="width: 100%;" cellpadding="0" cellspacing="0">
	<tr>
		<td><%=nHandle.wrapOpenLink(nComponentParameter, news)%></td>
		<td class="nnum" align="right"
			title="<%=news.getRemarks()%>#(news_layout.3), <%=news.getViews()%>#(news_layout.2)"><%=isNews ? "<span class=\"nnum\" style=\"color: red;\">"
						+ news.getRemarks() + "</span>"
						: news.getRemarks()%>/<%=news.getViews()%></td>
	</tr>
</table>
<%
	out.write(ContentLayoutUtils.layoutDesc(i++,
				nComponentParameter, news));
		out.write(ContentLayoutUtils.layoutTime(nComponentParameter,
				news, false));
%>
</div>
<%
	}
	if (ConvertUtils.toBoolean(request.getParameter("showMore"), true)) {
		String appUrl;
		final INewsApplicationModule module = NewsUtils.applicationModule;
		final int catalog = ConvertUtils.toInt(request
				.getParameter(module
						.getCatalogIdName(nComponentParameter)), -1);
		if (catalog > -1) {
			appUrl = module.getCatalogUrl(nComponentParameter, catalog);
		} else {
			appUrl = module.getApplicationUrl(nComponentParameter);
		}
%>
<div class="more"><a href="<%=appUrl%>">#(news_layout.1)</a></div>
<%
	}
%>
</div>