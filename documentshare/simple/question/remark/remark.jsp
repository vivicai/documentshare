<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@ page
	import="net.simpleframework.web.page.component.ComponentRenderUtils"%><%@page
	import="net.documentshare.question.QuestionBean"%><%@page
	import="net.documentshare.question.QuestionUtils"%>




<%
	final ComponentParameter nComponentParameter = new ComponentParameter(request, response, null);
	final QuestionBean questionBean = QuestionUtils.getQuestionBean(nComponentParameter);
%>
<div class="qremark">
	<%=ComponentRenderUtils.genParameters(nComponentParameter)%>
	<input type="hidden" id="questionId" name="questionId"
		value="<%=questionBean.getId()%>">
	<div id="qremark_list"></div>
	<%
		if (questionBean.isOpen()) {
	%>
	<div id="qremark_tb" class="tb">
		<div class="simple_toolbar1">
			<textarea id="textareaRemarkEditor" name="textareaRemarkEditor"
				rows="5"></textarea>
		</div>
		<table style="width: 100%;" cellpadding="0" cellspacing="0">
			<tr>
				<td valign="top">
					<div class="ins">
						<a class="emotion" onclick="$Actions['smileyRemarkDict']();">#(remark.12)</a>
					</div>
				</td>
				<td width="80" align="right">
					<span class="btn" id="submitRemarkEditor"
						onclick="$Actions['ajaxSave2Remark']();">确定</span>
				</td>
			</tr>
		</table>
		<div style="padding: 4px 8px;">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td id="remarkEditorValidateCode"></td>
					<td align="right" valign="middle">
						<a onclick="__remark_window(this);">#(remark.11)</a>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<%
		}
	%>
</div>