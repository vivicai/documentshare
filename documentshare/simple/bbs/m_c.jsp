<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.content.bbs.BbsUtils"%>
<%
	final String bbs_forum = BbsUtils.deployPath
			+ "jsp/bbs_forum_view.jsp";
%>
<div class="c_left"><jsp:include page="<%=bbs_forum%>"></jsp:include>
</div>
<div class="c_right"><jsp:include page="layout_left.jsp"></jsp:include>
</div>