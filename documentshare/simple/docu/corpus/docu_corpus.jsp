<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.simpleframework.util.ConvertUtils"%>
<%
	final int o = ConvertUtils.toInt(request.getParameter("o"), 0);
%>
<div class="simple_toolbar1">
	<%
		if (o == 1) {
	%>
	<jsp:include page="/simple/template/c.jsp" flush="true">
		<jsp:param value="/simple/docu/corpus/corpus_c.jsp" name="center" />
	</jsp:include>
	<%
		} else {
	%>
	<jsp:include page="/simple/template/lr.jsp" flush="true">
		<jsp:param value="/simple/docu/corpus/corpus_c.jsp" name="left" />
		<jsp:param value="/simple/docu/corpus/corpuslayout_right.jsp" name="center" />
	</jsp:include>
	<%
		}
	%>
</div>