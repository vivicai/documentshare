<%@page import="net.documentshare.docu.corpus.CorpusBean"%>
<%@page import="net.documentshare.docu.corpus.CorpusUtils"%>
<%@page import="net.simpleframework.content.news.NewsUtils"%>
<%@page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.ConvertUtils"%>
<%@ page import="net.simpleframework.content.ContentLayoutUtils"%><%@page
	import="net.simpleframework.core.ado.IDataObjectQuery"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.simpleframework.util.DateUtils"%>


<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	String type = requestResponse.getRequestParameter("type");
	final IDataObjectQuery<?> qs = CorpusUtils.queryCorpus(requestResponse);
%>
<div class="list_layout">
	<%
		CorpusBean bean;
		if (qs != null) {
			while ((bean = (CorpusBean) qs.next()) != null) {
				if (qs.position() > 6) {
					break;
				}
	%>
	<div class="rrow" style="padding-left: 0px;">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td><%=CorpusUtils.wrapOpenLink(bean)%></td>
				<td align="right" class="nnum" valign="top">
					<%
						if ("newest".equals(type)) {
									out.println(DateUtils.getRelativeDate(bean.getCreateDate()));
								} else if ("viewest".equals(type)) {
									out.println(bean.getReadTime());
								} else if ("recommend".equals(type)) {
									out.println(bean.getReadTime());
								}
					%>
				</td>
			</tr>
		</table>
	</div>
	<%
		}
		}
	%>
</div>