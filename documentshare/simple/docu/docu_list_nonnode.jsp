<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.simpleframework.util.ConvertUtils"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.simpleframework.ado.db.IQueryEntitySet"%><%@page
	import="net.documentshare.docu.DocuCatalog"%><%@page
	import="net.simpleframework.core.ado.IDataObjectQuery"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.simpleframework.content.ContentUtils"%><%@page
	import="net.simpleframework.util.DateUtils"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final int catalogId = ConvertUtils.toInt(request.getParameter("catalogId"), 0);
	final IQueryEntitySet<DocuCatalog> qs = DocuUtils.applicationModule.queryCatalogs(catalogId);
	if (qs == null)
		return;
%>

<style>
.block_layout3 .t {
	text-align: left;
	border-bottom: 1px solid #CCC;
	color: black;
	font-weight: bold;
}

#docunonListId .rrow1 {
	
}

.list_layout .rrow {
	padding: 2px 4px 0 0px;
}
</style>

<div class=common_catalog style="margin-bottom: 6px;">
	<div class="c">
		<%=DocuUtils.catalogView2(requestResponse)%></div>
</div>
<div id="docunonListId">
	<%
		final int t = ConvertUtils.toInt(requestResponse.getRequestParameter("t"), 0);
		final int s = ConvertUtils.toInt(requestResponse.getRequestParameter("s"), 0);
		final int od = ConvertUtils.toInt(requestResponse.getRequestParameter("od"), 0);
		final StringBuffer param = new StringBuffer();
		param.append(t).append("-").append(s).append("-").append(od).append("-");
		DocuCatalog dc = null;
		while ((dc = qs.next()) != null) {
			IDataObjectQuery<?> docuQs = DocuUtils.queryDocuNon(requestResponse, "all", "view", dc.getId());
			if (docuQs.getCount() == 0) {
				continue;
			}
	%>
	<div class="rrow1 common_catalog" style="margin-bottom: 6px;">
		<div class="c">
			<div class="ctitle ctitle_border_radius">
				<a href="/docu/<%=param.toString()%><%=dc.getId()%>.html"><%=dc.getText()%></a>
			</div>
			<div style="border-top: 1px solid #ccc;">
				<table>
					<tr>
						<td width="200px;" valign="top">
							<div class="block_layout3">
								<%
									final IQueryEntitySet<DocuCatalog> catalogQs = DocuUtils.applicationModule.queryCatalogs(dc.getId());
										if (catalogQs != null && catalogQs.getCount() > 0) {
								%>
								<div class="t" style="color: #9D2703">
									目录子类
								</div>
								<div class="wrap_text" style="padding-bottom: 20px;">
									<%
										DocuCatalog dc1 = null;
												while ((dc1 = catalogQs.next()) != null) {
									%>
									<a href="/docu/<%=param.toString()%><%=dc1.getId()%>.html"><%=dc1.getText()%></a>
									<%
										}
									%>
								</div>
								<%
									}
								%>
								<div class="t" style="color: #9D2703;">
									热门文档
								</div>
								<div class="wrap_text">
									<div class="list_layout" style="padding-top: 4px;">
										<%
											DocuBean docuBean;
												while ((docuBean = (DocuBean) docuQs.next()) != null) {
													if (docuQs.position() >= 6) {
														break;
													}
										%>
										<div class="rrow">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<td width="20" valign="top">
														<img alt="<%=docuBean.getExtension()%>"
															src="<%=DocuUtils.getFileImage(requestResponse, docuBean)%>">
													</td>
													<td>
														<a target="_blank" title="<%=docuBean.getTitle()%>"
															href="/docu/<%=docuBean.getId()%>.html"><%=ItSiteUtil.getShortString(docuBean.getTitle(), 20, true)%></a>
													</td>
												</tr>
											</table>
										</div>
										<%
											}
										%>
									</div>
								</div>

							</div>
						</td>
						<td>
							<div class="hot-img" style="margin-top: 10px;">
								<%
									docuQs = DocuUtils.queryDocuNon(requestResponse, "docu", "recommended", dc.getId());
										while ((docuBean = (DocuBean) docuQs.next()) != null) {
											if (docuQs.position() >= 4) {
												break;
											}
								%>
								<dl class="mt5">
									<dt>
										<a
											href="<%=DocuUtils.applicationModule.getViewUrl(docuBean.getId())%>"
											target="_blank"><img
												src="<%=DocuUtils.getPageImgSrc(requestResponse, docuBean)%>"
												title="<%=docuBean.getTitle()%>"
												alt="<%=docuBean.getTitle()%>"> </a>
										<span class="icon1" title="<%=docuBean.getExtension()%>"
											style="background-image: url('<%=DocuUtils.getFileImage(requestResponse, docuBean)%>');"></span>
									</dt>
									<dd>
										<a
											href="<%=DocuUtils.applicationModule.getViewUrl(docuBean.getId())%>"
											target="_blank"><%=ItSiteUtil.getShortString(docuBean.getTitle(), 20, true)%></a>
									</dd>
									<dd class="f-grey">
										阅读:<%=docuBean.getViews()%>
									</dd>
									<dd class="f-grey">
										发布:<%=DateUtils.getRelativeDate(docuBean.getCreateDate())%>
									</dd>
								</dl>
								<%
									}
								%>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<%
		}
	%>
</div>
