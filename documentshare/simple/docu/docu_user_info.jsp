<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.simpleframework.organization.IUser"%><%@page
	import="net.simpleframework.content.ContentUtils"%><%@page
	import="net.simpleframework.util.ConvertUtils"%><%@page
	import="net.simpleframework.util.IoUtils"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.simpleframework.util.HTMLBuilder"%><%@page
	import="net.documentshare.docu.EDocuFunction"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final DocuBean docuBean = DocuUtils.applicationModule.getViewDocuBean(requestResponse);
	if (docuBean == null) {
		return;
	}
	final IUser user = ItSiteUtil.getUserById(docuBean.getUserId());
%>
<div>
	<table>
		<tr>
			<td valign="top"><%=ContentUtils.getAccountAware().wrapImgAccountHref(requestResponse, user)%></td>
			<td valign="top">
				<table>
					<tr>
						<td align="right">
							小编：
						</td>
						<td colspan="3"><%=user.getText()%></td>
					</tr>
					<tr>
						<td align="right">
							时间：
						</td>
						<td colspan="3"><%=ConvertUtils.toDateString(docuBean.getCreateDate(), "yyyy-MM-dd")%></td>
					</tr>
					<tr>
						<td align="right">
							下载：
						</td>
						<td class="nnum">
							<a
								onclick="$Actions['downUsersWindowAct']('docuId=<%=docuBean.getId()%>');"><%=docuBean.getDownCounter()%></a>
						</td>
						<td align="right">
							阅读：
						</td>
						<td class="nnum"><%=docuBean.getViews()%></td>
					</tr>
					<tr>
						<td align="right">
							积分：
						</td>
						<td class="nnum">
							<%=docuBean.getPoint()%>
						</td>
						<td align="right">
							大小：
						</td>
						<td class="nnum"><%=IoUtils.toFileSize(docuBean.getFileSize())%></td>
					</tr>
					<tr>
						<td colspan="4">
							<table id="gradeForm">
								<tr>
									<td nowrap="nowrap">
										<span class="nnum docuStar" id="docuStar"><%=docuBean.getTotalGrade()%></span>
									</td>
									<td width="100%">
										<div class="shop-rating" id="shop-rating">
											<span class='m_star m_<%=docuBean.getStarGrade()%>'
												id="m_star"
												onmouseover="this.$toggle();$('stars2').$toggle();"></span>
											<ul class="rating-level" id="stars2" style="display: none;"
												onclick="this.$toggle();$('m_star').$toggle();">
												<li>
													<a class="one-star" star:value="1" href="#">20</a>
												</li>
												<li>
													<a class="two-stars" star:value="2" href="#">40</a>
												</li>
												<li>
													<a class="three-stars" star:value="3" href="#">60</a>
												</li>
												<li>
													<a class="four-stars" star:value="4" href="#">80</a>
												</li>
												<li>
													<a class="five-stars" star:value="5" href="#">100</a>
												</li>
											</ul>
											<span class="result" id="stars2-tips"></span>
											<input type="hidden" id="stars2-input" name="grade" value="" />
											<input type="hidden" id="docuId" name="docuId"
												value="<%=docuBean.getId()%>" />
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<img src="/simple/docu/images/download.png" style="cursor: pointer;"
					onclick="$Actions['docuDownloadAct']('docuId=<%=docuBean.getId()%>');"
					alt="下载文档">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<%
					if (StringUtils.hasText(docuBean.getContent())) {
				%>
				<span id="dc_s"> <%=ItSiteUtil.getShortString(docuBean.getContent(), 300, true)%>
					<%
						if (docuBean.getContent().length() > 300) {
					%> <a onclick="$('dc_s').$toggle();$('dc_m').$toggle();">显示更多»</a>
				</span>

				<span id="dc_m" style="display: none;"> <%=docuBean.getContent()%>
					<a onclick="$('dc_s').$toggle();$('dc_m').$toggle();">显示较少»</a> </span>
				<%
					}
					}
				%>
			</td>
		</tr>
	</table>
</div>
<script type="text/javascript">
var MStars = {
	create : function() {
		return function() {
			this.initialize.apply(this, arguments);
		}
	}
}
var Extend = function(destination, source) {
	for ( var property in source) {
		destination[property] = source[property];
	}
}
function stopDefault(e) {
	if (e && e.preventDefault) {
		e.preventDefault();
	} else {
		window.event.returnValue = false;
	}
	return false;
}
var Stars = MStars.create();
Stars.prototype = {
	initialize : function(star, options) {
		this.SetOptions(options); //默认属性
	var flag = 999; //定义全局指针
	var isIE = (document.all) ? true : false; //IE?
	var starlist = document.getElementById(star).getElementsByTagName('a'); //星星列表
	var input = document.getElementById(this.options.Input)
			|| document.getElementById(star + "-input"); // 输出结果
	var tips = document.getElementById(this.options.Tips)
			|| document.getElementById(star + "-tips"); // 打印提示
	var nowClass = " " + this.options.nowClass; // 定义选中星星样式名
	var tipsTxt = this.options.tipsTxt; // 定义提示文案
	var len = starlist.length; //星星数量

	for (i = 0; i < len; i++) { // 绑定事件 点击 鼠标滑过
		starlist[i].value = i;
		starlist[i].onclick = function(e) {
			stopDefault(e);
			this.className = this.className + nowClass;
			flag = this.value;
			input.value = this.getAttribute("star:value");
			tips.innerHTML = tipsTxt[this.value]
			$IT.A('docuStarAct');
		}
		starlist[i].onmouseover = function() {
			if (flag < 999) {
				var reg = RegExp(nowClass, "g");
				starlist[flag].className = starlist[flag].className.replace(
						reg, "")
			}
		}
		starlist[i].onmouseout = function() {
			if (flag < 999) {
				starlist[flag].className = starlist[flag].className + nowClass;
			}
		}
	}
	;
	if (isIE) { //FIX IE下样式错误
		var li = document.getElementById(star).getElementsByTagName('li');
		for ( var i = 0, len = li.length; i < len; i++) {
			var c = li[i];
			if (c) {
				c.className = c.getElementsByTagName('a')[0].className;
			}
		}
	}
},
//设置默认属性
	SetOptions : function(options) {
		this.options = {//默认值
			Input : "",//设置触保存分数的INPUT
			Tips : "",//设置提示文案容器
			nowClass : "current-rating",//选中的样式名
			tipsTxt : [ "很差", "较差", "还行", "推荐", "力荐" ]
		//提示文案
		};
		Extend(this.options, options || {});
	}
}
new Stars("stars2");
</script>