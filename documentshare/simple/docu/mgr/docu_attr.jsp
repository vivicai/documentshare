<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="net.simpleframework.util.StringUtils"%>
<%@page import="net.simpleframework.content.EContentType"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@page import="net.documentshare.docu.EDocuStatus"%>
<div id="__docuAttrForm2">
	<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="l">
				自动审核
			</td>
			<td>
				<input type="checkbox" id="autoAudit" name="autoAudit" value="true">
			</td>
		</tr>
		<tr>
			<td class="l">
				转换页数
			</td>
			<td>
				<input type="text" " id="converNumber" name="converNumber">
			</td>
		</tr>
	</table>
	<div style="text-align: right; margin-top: 6px;">
		<input type="button" class="button2" value="保存" id="docuAttrSaveActButton"
			onclick="$IT.A('docuAttrSaveAct');" />
		<input type="button" value="取消" onclick="$IT.C('docuAttrWindowAct');" />
	</div>
</div>
<style type="text/css">
#__docuAttrForm2 .l {
	width: 70px;
	text-align: right;
}

#__docuAttrForm2 {
	background: #f7f7ff;
	border: 1px solid #ddd;
	padding: 6px 8px;
}
</style>
