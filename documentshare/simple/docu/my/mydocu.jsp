<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.simpleframework.content.EContentStatus"%><%@page
	import="net.documentshare.docu.EDocuFunction"%>
<%
	final String id = StringUtils.text(request.getParameter("id"), "myAll");
%>
<jsp:include page="/simple/docu/my/mydocu_c.jsp" flush="true"></jsp:include>
<style>
.displayOut {
	color: blue;
}
</style>
<script type="text/javascript">
$ready(function() {
	refreshDocu('<%=id%>');
	if ('myNotAudit' == '<%=id%>') {
		$Actions['myDocuTableNonAct']
				('docu_status=<%=EContentStatus.audit.name()%>');
	} else if ('myUpload' == '<%=id%>') {
		$Actions['myUploadAct']();
	} else if ('myAttention' == '<%=id%>') {
		$Actions['myAttentionAct']('');
	} else {
		$Actions['myDocuTableAct']('docu_type=myAll');
	}
});
</script>