<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page import="net.simpleframework.my.message.MessageUtils"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(
			request, response);
	final String my_message = MessageUtils.deployPath
			+ "jsp/my_message.jsp";
%>
<%=MessageUtils.tabs(requestResponse)%>
<div class="simple_toolbar3" style="width: 75%; margin: 8px 0px;">
	<div style="border-bottom: 3px double #ddd; padding-bottom: 6px; margin-bottom: 4px;">
		<input type="button" class="button2" value="发送短消息" onclick="$Actions['myMessageSentWindow']();" />
	</div>
	<jsp:include page="<%=my_message%>" flush="true"></jsp:include>
</div>