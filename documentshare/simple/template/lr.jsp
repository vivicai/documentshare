<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.web.WebUtils"%>
<%@ page import="net.simpleframework.util.StringUtils"%>
<html>
	<jsp:include page="inc_head.jsp" flush="true"></jsp:include>
	<body style="margin: 0px; padding: 0px;">
		<div align="center">
			<div id="t_main">
				<div id="t_header"><jsp:include page="header.jsp" flush="true"></jsp:include></div>
				<div id="t_body" class="clear_float">
					<table width="100%" height="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td width="22%" valign="top">
								<div class="c_left">
									<%
										final String left = WebUtils.putIncludeParameters(request, request.getParameter("left"));
										final String center = WebUtils.putIncludeParameters(request, request.getParameter("center"));
										if (StringUtils.hasText(left)) {
									%><jsp:include page="<%=left%>" flush="true"></jsp:include>
									<%
										}
									%>
								</div>
							</td>
							<td valign="top">
								<div class="c_right">
									<%
										if (StringUtils.hasText(center)) {
									%><jsp:include page="<%=center%>" flush="true"></jsp:include>
									<%
										}
									%>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<%
					final String footer = request.getParameter("footer");
					if (StringUtils.hasText(footer)) {
				%><jsp:include page="<%=footer%>" flush="true"></jsp:include>
				<%
					}
				%>
				<jsp:include page="/simple/template/footer.jsp" flush="true"></jsp:include>
			</div>
		</div>
	</body>
</html>



