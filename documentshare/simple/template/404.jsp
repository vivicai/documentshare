<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
	<head>
		<style>
.error {
	width: 502px;
	border: 1px solid #DDD;
	border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	padding: 4px;
}

.error .h {
	border-bottom: 1px dashed #DDD;
}
</style>
	</head>
	<body>
		<table style="width: 100%; height: 100%;">
			<tr>
				<td align="center">
					<table class="error">
						<tr>
							<td class="h">
								<img
									src="<%=request.getContextPath()%>/simple/template/images/404.jpg">
							</td>
						</tr>
						<tr>
							<td align="center">
								<input type="button" value="返回首页"
									onclick="window.location='/';" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>