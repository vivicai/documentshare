<%@page import="net.simpleframework.content.ContentUtils"%>
<%@page import="net.simpleframework.content.news.NewsUtils"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.core.ado.IDataObjectQuery"%>
<%@ page import="net.simpleframework.util.ConvertUtils"%>
<%@ page import="net.simpleframework.content.ContentLayoutUtils"%>
<%@page import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.simpleframework.web.WebUtils"%><%@page
	import="net.documentshare.docu.EDocuFunction"%><%@page import="net.simpleframework.util.DateUtils"%>


<%
	final String _docu_type = WebUtils.toLocaleString(request.getParameter("_docu_type"));
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final IDataObjectQuery<?> qs = DocuUtils.queryDocu(requestResponse, null, _docu_type,"");
	if (qs == null) {
		return;
	}
%>
<div class="list_layout" style="float: left;display: block;width: 100%;">
	<%
		DocuBean docuBean;
		while ((docuBean = (DocuBean) qs.next()) != null) {
			if (qs.position() > 9) {
				break;
			}
	%>
	<div class="rrow" style="padding-left: 0px;float: left;width: 48%;" >
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20">
					<img alt="<%=docuBean.getExtension()%>"
						src="<%=DocuUtils.getFileImage(requestResponse, docuBean)%>">
				</td>
				<td>
					<a
						href="<%=DocuUtils.applicationModule.getViewUrl(docuBean.getId())%>" title="<%=docuBean.getTitle() %>"
						target="_blank"><%=ItSiteUtil.getShortString(docuBean.getTitle(),25,true)%></a>
				</td>
			</tr>
		</table>
	</div>
	<%
		}
	%>
</div>
