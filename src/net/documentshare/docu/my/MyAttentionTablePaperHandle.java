package net.documentshare.docu.my;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.documentshare.docu.AbstractDocuTablePagerData;
import net.documentshare.docu.DocuAttention;
import net.documentshare.docu.DocuBean;
import net.documentshare.docu.DocuUtils;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.my.space.MySpaceUtils;
import net.simpleframework.organization.IJob;
import net.simpleframework.organization.OrgUtils;
import net.simpleframework.organization.account.IGetAccountAware;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractTablePagerData;
import net.simpleframework.web.page.component.ui.pager.TablePagerColumn;
import net.simpleframework.web.page.component.ui.pager.db.AbstractDbTablePagerHandle;

public class MyAttentionTablePaperHandle extends AbstractDbTablePagerHandle {
	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("jobExecute".equals(beanProperty)) {
			return IJob.sj_account_normal;
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(ComponentParameter compParameter) {
		final ITableEntityManager tMgr = DocuUtils.applicationModule.getDataObjectManager(DocuAttention.class);
		final List<Object> ol = new ArrayList<Object>();
		final StringBuffer sql = new StringBuffer();
		sql.append(" userId=?");
		final IGetAccountAware accountAware = MySpaceUtils.getAccountAware();
		if (accountAware.isMyAccount(compParameter)) {
			ol.add(ItSiteUtil.getLoginUser(compParameter).getId());
		} else {
			ol.add(compParameter.getRequestParameter(OrgUtils.um().getUserIdParameterName()));
		}
		sql.append(" order by createDate desc");
		return tMgr.query(new ExpressionValue(sql.toString(), ol.toArray(new Object[] {})), DocuAttention.class);
	}

	@Override
	public AbstractTablePagerData createTablePagerData(final ComponentParameter compParameter) {
		return new AbstractDocuTablePagerData(compParameter) {

			@Override
			public Map<String, TablePagerColumn> getTablePagerColumns() {
				final Map<String, TablePagerColumn> column = new LinkedHashMap<String, TablePagerColumn>(super.getTablePagerColumns());
				final IGetAccountAware accountAware = MySpaceUtils.getAccountAware();
				if (!accountAware.isMyAccount(compParameter)) {
					column.remove("action");
				}
				return column;
			}

			@Override
			protected Map<Object, Object> getRowData(final Object arg0) {
				final DocuAttention attentionBean = (DocuAttention) arg0;
				final DocuBean docuBean = DocuUtils.applicationModule.getBean(DocuBean.class, attentionBean.getDocuId());
				final Map<Object, Object> rowData = new LinkedHashMap<Object, Object>();
				if (docuBean == null)
					return null;
				rowData.put("title", buildTitle(docuBean));
				rowData.put("docu", docuBean.getDocuFunction().toString());
				rowData.put("action", "<a onclick=\"$Actions['docuAttentionAct']('docuId=" + docuBean.getId() + "');\">取消收藏</a>");
				return rowData;
			}
		};
	}

	@Override
	public Map<String, Object> getFormParameters(ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, OrgUtils.um().getUserIdParameterName());
		return parameters;
	}

}
