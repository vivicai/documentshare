package net.documentshare.docu;

import java.util.ArrayList;
import java.util.Collection;

import net.simpleframework.ado.db.IQueryEntitySet;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.tree.AbstractTreeHandle;
import net.simpleframework.web.page.component.ui.tree.AbstractTreeNode;
import net.simpleframework.web.page.component.ui.tree.TreeBean;
import net.simpleframework.web.page.component.ui.tree.TreeNode;

/**
 * 显示查询的树结构
 */
public class DocuCodeCatalogAddTreeHandle extends AbstractTreeHandle {

	@Override
	public Collection<? extends AbstractTreeNode> getTreenodes(final ComponentParameter compParameter, final AbstractTreeNode treeNode) {
		final Collection<TreeNode> nodes = new ArrayList<TreeNode>();
		if (treeNode == null) {
			final IQueryEntitySet<DocuCodeCatalog> catalogs = DocuUtils.applicationModule.queryCodeCatalogs(null);
			DocuCodeCatalog wc = null;
			while ((wc = catalogs.next()) != null) {
				final TreeNode treeNode2 = new TreeNode((TreeBean) compParameter.componentBean, treeNode, wc);
				treeNode2.setText(wc.getText());
				treeNode2.setId(wc.getId().toString());
				nodes.add(treeNode2);
			}
			return nodes;
		}
		return nodes;
	}
}
