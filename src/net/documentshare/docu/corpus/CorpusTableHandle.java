package net.documentshare.docu.corpus;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import net.documentshare.docu.DocuBean;
import net.documentshare.docu.DocuCatalog;
import net.documentshare.docu.DocuUtils;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.ado.db.SQLValue;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.organization.IJob;
import net.simpleframework.organization.IUser;
import net.simpleframework.organization.OrgUtils;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.IoUtils;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ado.AbstractDbComponentHandle;
import net.simpleframework.web.page.component.ui.pager.AbstractTablePagerData;
import net.simpleframework.web.page.component.ui.pager.AbstractTablePagerHandle;
import net.simpleframework.web.page.component.ui.pager.TablePagerColumn;
import net.simpleframework.web.page.component.ui.pager.db.AbstractDbTablePagerAction;
import net.simpleframework.web.page.component.ui.pager.db.AbstractDbTablePagerHandle;

public class CorpusTableHandle extends AbstractDbTablePagerHandle {
	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("jobView".equals(beanProperty)) {
			return IJob.sj_account_normal;
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(final ComponentParameter compParameter) {
		String userId = compParameter.getRequestParameter(OrgUtils.um().getUserIdParameterName());
		String type = compParameter.getRequestParameter("type");
		if (StringUtils.hasText(type) && type.equals("collect")) {
			StringBuffer sql = new StringBuffer("select c.* from it_docu_corpus c,it_corpus_attention ca where c.id=ca.corpusId and ca.userId="
					+ userId);
			return DocuUtils.applicationModule.getDataObjectManager(CorpusBean.class).query(new SQLValue(sql.toString()), CorpusBean.class);
		}
		return DocuUtils.applicationModule.queryBean("userId=?", new Object[] { userId }, CorpusBean.class);
	}

	@Override
	public Map<String, Object> getFormParameters(ComponentParameter compParameter) {
		// TODO Auto-generated method stub
		Map<String, Object> param = super.getFormParameters(compParameter);
		String type = compParameter.getRequestParameter("type");
		param.put("type", type);
		String userid = compParameter.getRequestParameter("userid");
		param.put("userid", userid);
		return param;
	}

	@Override
	public AbstractTablePagerData createTablePagerData(final ComponentParameter compParameter) {

		return new AbstractTablePagerData(compParameter) {
			@Override
			public Map<String, TablePagerColumn> getTablePagerColumns() {

				Map<String, TablePagerColumn> columnN = super.getTablePagerColumns();
				Map<String, TablePagerColumn> column = new LinkedHashMap<String, TablePagerColumn>();
				column.putAll(columnN);
				if (!ItSiteUtil.isLogin(compParameter)) {
					column.remove("action");
				}
				String type = compParameter.getRequestParameter("type");
				if (StringUtils.hasText(type) && type.equals("collect")) {
					column.remove("action");
				}
				return column;
			}

			@Override
			protected Map<Object, Object> getRowData(Object dataObject) {
				CorpusBean bean = (CorpusBean) dataObject;
				Map<Object, Object> data = new HashMap<Object, Object>();
				final Map<Object, Object> rowData = new LinkedHashMap<Object, Object>();
				rowData.put("title", buildTitle(bean, compParameter));
				final DocuCatalog catalog = DocuUtils.applicationModule.getBean(DocuCatalog.class, bean.getCatalogId());
				rowData.put("catalogId", catalog == null ? "" : catalog.getText());
				rowData.put("createDate", ConvertUtils.toDateString(bean.getCreateDate(), "yyyy-MM-dd HH"));
				rowData.put("action", "<a class='myCB1 down_menu_image'></a>");
				return rowData;
			}

		};
	}

	public String buildTitle(final CorpusBean corpusBean, ComponentParameter compParameter) {
		final StringBuffer sb = new StringBuffer();
		sb.append("<table><tr class='album_doc'>");
		sb.append("<td class='albumImg' valign='top'>");
		sb.append("<img class='docuPage' src='" + CorpusUtils.getCorpusFrontImage(corpusBean, compParameter) + "'>");
		sb.append("</td>");
		sb.append("<td valign='top' width=\"100%\">");
		sb.append("<table>");
		sb.append("<tr><td>");
		sb.append("<div class=\"f4\" style=\"margin: 4px 0px;\">");
		sb.append(CorpusUtils.wrapOpenLink(corpusBean));
		sb.append("</div>");
		sb.append("</td></tr>");
		sb.append("<tr><td valign=\"top\">");
		sb.append("<table>");
		sb.append("<tr>");
		sb.append("<td align=\"right\">");
		sb.append("阅读:");
		sb.append("</td>");
		sb.append("<td>");
		sb.append(wrapNum(corpusBean.getReadTime()));
		sb.append("</td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("<td align=\"right\">");
		sb.append("文件:");
		sb.append("</td>");
		sb.append("<td>");
		sb.append(wrapNum(DocuUtils.applicationModule.queryBean("CorpusId=" + corpusBean.getId().getValue(), CorpusOfDocBean.class).getCount()));
		sb.append("</td>");
		sb.append("</tr>");
		sb.append("</table>");
		sb.append("</td>");
		sb.append("</tr></table>");
		sb.append("</td></tr></table>");
		return sb.toString();
	}

	protected String wrapNum(final Object num) {
		final StringBuilder sb = new StringBuilder();
		sb.append("<span class=\"nnum\">").append(num).append("</span>");
		return sb.toString();
	}
}
