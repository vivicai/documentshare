package net.documentshare.docu.corpus;

import java.util.List;
import java.util.Map;

import net.documentshare.docu.DocuCatalog;
import net.documentshare.docu.DocuUtils;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.DefaultPageHandle;
import net.simpleframework.web.page.PageParameter;

public class CorpusPageLoadHandle extends DefaultPageHandle {

	public void corpusInfoLoad(PageParameter pageParameter, Map<String, Object> dataBinding, List<String> visibleToggleSelector,
			List<String> readonlySelector, List<String> disabledSelector) {
		String corpusId = pageParameter.getRequestParameter("corpusId");
		if (StringUtils.hasText(corpusId)) {
			CorpusBean bean = DocuUtils.applicationModule.getBean(CorpusBean.class, corpusId);
			dataBinding.put("corpus_title", bean.getName());
			dataBinding.put("description", bean.getDescription());
			dataBinding.put("corpos_Id", corpusId);
			dataBinding.put("docu_catalog", bean.getCatalogId());
			final StringBuffer catalogTitle = new StringBuffer();
			final DocuCatalog catalog = DocuUtils.applicationModule.getBean(DocuCatalog.class, bean.getCatalogId());
			if (catalog != null) {
				final DocuCatalog pCatalog = (DocuCatalog) catalog.parent(DocuUtils.applicationModule);
				if (pCatalog != null) {
					catalogTitle.append(pCatalog.getText()).append("-");
				}
				catalogTitle.append(catalog.getText()).append("-");
			}
			dataBinding.put("docu_catalog_text", catalogTitle.toString());
			dataBinding.put("docu_catalog", bean.getCatalogId().getValue());
		}

	}

}
