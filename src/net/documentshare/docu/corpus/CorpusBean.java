package net.documentshare.docu.corpus;

import java.util.Date;

import net.simpleframework.content.EContentType;
import net.simpleframework.core.bean.AbstractIdDataObjectBean;
import net.simpleframework.core.id.ID;

public class CorpusBean extends AbstractIdDataObjectBean {
	private String name;
	private String description;
	private ID userId;
	private ID catalogId;
	private ID frontCover;
	private Date createDate;
	private Date updateDate;

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	private long readTime;
	private EContentType contentType;
	private long attentions;// 关注

	public long getAttentions() {
		return attentions;
	}

	public void setAttentions(long attentions) {
		this.attentions = attentions;
	}

	public ID getUserId() {
		return userId;
	}

	public long getReadTime() {
		return readTime;
	}

	public void setReadTime(long readTime) {
		this.readTime = readTime;
	}

	public EContentType getContentType() {
		return contentType;
	}

	public void setContentType(EContentType contentType) {
		this.contentType = contentType;
	}

	public void setUserId(ID userId) {
		this.userId = userId;
	}

	public ID getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(ID catalogId) {
		this.catalogId = catalogId;
	}

	public ID getFrontCover() {
		return frontCover;
	}

	public void setFrontCover(ID frontCover) {
		this.frontCover = frontCover;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
