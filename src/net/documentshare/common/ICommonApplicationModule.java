package net.documentshare.common;

import net.documentshare.i.IItSiteApplicationModule;

public interface ICommonApplicationModule extends IItSiteApplicationModule {
	CommonBean getCommonBean(final ECommonType type);
}
