package net.documentshare.common;

import net.simpleframework.core.AbstractInitializer;
import net.simpleframework.core.IApplication;
import net.simpleframework.core.IInitializer;

public class CommonInitializer extends AbstractInitializer {
	private ICommonApplicationModule applicationModule;

	@Override
	public void doInit(IApplication application) {
		try {
			super.doInit(application);
			applicationModule = new CommonApplicationModule();
			applicationModule.init(this);
			IInitializer.Utils.deploySqlScript(getClass(), application, CommonUtils.deployPath);
		} catch (Exception e) {
		}
	}

}
