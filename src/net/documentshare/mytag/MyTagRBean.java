package net.documentshare.mytag;

import net.simpleframework.core.bean.AbstractDataObjectBean;
import net.simpleframework.core.id.ID;

public class MyTagRBean extends AbstractDataObjectBean {
	private ID tagId;

	private ID rid;

	public ID getTagId() {
		return tagId;
	}

	public void setTagId(final ID tagId) {
		this.tagId = tagId;
	}

	public ID getRid() {
		return rid;
	}

	public void setRid(final ID rid) {
		this.rid = rid;
	}

	private static final long serialVersionUID = -4698059815398303527L;
}
