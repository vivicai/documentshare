package net.documentshare.mytag;

import java.util.Map;

import net.simpleframework.ado.IDataObjectValue;
import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.event.TableEntityAdapter;
import net.simpleframework.applets.tag.TagRBean;
import net.simpleframework.content.EContentType;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.util.LocaleI18n;
import net.simpleframework.web.page.IForward;
import net.simpleframework.web.page.UrlForward;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.base.ajaxrequest.AbstractAjaxRequestHandle;

public class MyTagAction extends AbstractAjaxRequestHandle {
	@Override
	public Object getBeanProperty(final ComponentParameter compParameter, final String beanProperty) {
		if ("jobExecute".equals(beanProperty)) {
			final String ajaxComponentName = compParameter.componentBean.getName();
			if ("ajaxTagsManagerPage".equals(ajaxComponentName) || "ajaxTagRPage".equals(ajaxComponentName)) {
				return MyTagUtils.applicationModule.getManager(compParameter);
			}
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	public IForward tagDelete(final ComponentParameter compParameter) {
		final String tag = compParameter.getRequestParameter(IMyTagApplicationModule._TAG_ID);
		return jsonForward(compParameter, new JsonCallback() {
			@Override
			public void doAction(final Map<String, Object> json) {
				MyTagUtils.getTableEntityManager(MyTagBean.class).deleteTransaction(
						new ExpressionValue("id=?", new Object[] { tag }), new TableEntityAdapter() {
							@Override
							public void afterDelete(final ITableEntityManager manager, final IDataObjectValue dataObjectValue) {
								MyTagUtils.getTableEntityManager(MyTagRBean.class).delete(
										new ExpressionValue("tagid=?", new Object[] { tag }));
							}
						});
			}
		});
	}

	public IForward tagRDelete(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {
			@Override
			public void doAction(final Map<String, Object> json) {
				MyTagUtils.getTableEntityManager(TagRBean.class).delete(
						new ExpressionValue("tagid=? and rid=?", new Object[] { compParameter.getRequestParameter("t"),
								compParameter.getRequestParameter("c") }));
			}
		});
	}

	public IForward tagsManagerUrl(final ComponentParameter compParameter) {
		return new UrlForward(MyTagUtils.deploy + "/tags_manager.jsp");
	}

	public IForward tagRUrl(final ComponentParameter compParameter) {
		return new UrlForward(MyTagUtils.deploy + "/tags_r.jsp");
	}

	public IForward tagRebuild(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {
			@Override
			public void doAction(final Map<String, Object> json) {
				MyTagUtils.applicationModule.reCreateAllTags(compParameter);
				json.put("result", LocaleI18n.getMessage("TagAction.0"));
			}
		});
	}

	public IForward tagOptionSave(final ComponentParameter compParameter) {
		return jsonForward(compParameter, new JsonCallback() {
			@Override
			public void doAction(final Map<String, Object> json) {
				final ITableEntityManager temgr = MyTagUtils.getTableEntityManager();
				final MyTagBean tag = temgr.queryForObjectById(compParameter.getRequestParameter(IMyTagApplicationModule._TAG_ID), MyTagBean.class);
				if (tag != null) {
					tag.setTtype(ConvertUtils.toEnum(EContentType.class, compParameter.getRequestParameter("to_type")));
					temgr.update(new Object[] { "ttype" }, tag);
				}
			}
		});
	}
}
