package net.documentshare.mytag;

import net.simpleframework.core.AbstractInitializer;
import net.simpleframework.core.IApplication;
import net.simpleframework.core.IInitializer;

/**
 * 问答
 * @author lg
 *
 */
public class MyTagInitializer extends AbstractInitializer {
	private IMyTagApplicationModule tagApplicationModule;

	@Override
	public void doInit(IApplication application) {
		try {
			super.doInit(application);
			tagApplicationModule = new DefaultMyTagApplicationModule();
			tagApplicationModule.init(this);
			IInitializer.Utils.deploySqlScript(getClass(), application, MyTagUtils.deployPath);
		} catch (Exception e) {
		}
	}
}
