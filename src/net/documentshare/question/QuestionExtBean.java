package net.documentshare.question;

import java.util.Date;

import net.simpleframework.core.bean.AbstractIdDataObjectBean;
import net.simpleframework.core.id.ID;

/**
 * 问题补充
 */
public class QuestionExtBean extends AbstractIdDataObjectBean {
	private ID questionId;
	private String content;//补充说明
	private Date createDate;//创建时间

	public QuestionExtBean() {
		this.createDate = new Date();
	}

	public ID getQuestionId() {
		return questionId;
	}

	public void setQuestionId(ID questionId) {
		this.questionId = questionId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
