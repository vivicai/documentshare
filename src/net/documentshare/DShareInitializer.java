package net.documentshare;

import net.documentshare.common.CommonInitializer;
import net.documentshare.docu.DocuInitializer;
import net.documentshare.documentconfig.DocumentConfigMgr;
import net.documentshare.mytag.MyTagInitializer;
import net.documentshare.question.QuestionInitializer;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.core.AbstractInitializer;
import net.simpleframework.core.IApplication;
import net.simpleframework.util.IoUtils;
import net.simpleframework.web.IWebApplication;

public class DShareInitializer extends AbstractInitializer {

	@Override
	public void doInit(IApplication application) {
		super.doInit(application);
		try {
			doInitInitializer(application);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void doInitInitializer(IApplication application) {
		new DocuInitializer().doInit(application);
		new MyTagInitializer().doInit(application);
		new QuestionInitializer().doInit(application);
		new CommonInitializer().doInit(application);
		try {
			if (application instanceof IWebApplication) {
				final IWebApplication webApplication = ((IWebApplication) application);
				final String filePath = webApplication.getServletContext().getRealPath("") + "/config/document.xml";
				DocumentConfigMgr.getDocuMgr().initDocuMgr(filePath);
			}

			Thread.sleep(5000);
			System.out.println("---");
			IoUtils.unzip(getClass().getResourceAsStream("$resource.zip"), application.getApplicationAbsolutePath("$resource"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected Class<?>[] getI18n() {
		return new Class<?>[] { ItSiteUtil.class };
	}

}
