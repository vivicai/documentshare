package net.simpleframework.web.page.component.ui.htmleditor;

import net.simpleframework.web.page.PageDocument;
import net.simpleframework.web.page.component.AbstractContainerBean;
import net.simpleframework.web.page.component.IComponentRegistry;

import org.dom4j.Element;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class HtmlEditorBean extends AbstractContainerBean {
	private EToolbar toolbar;

	private String textarea;

	private boolean toolbarCanCollapse = true;

	private boolean resizeEnabled;

	private boolean startupFocus = true;

	private ELineMode enterMode, shiftEnterMode;

	private String htmlContent;

	private String jsLoadedCallback;

	public HtmlEditorBean(final IComponentRegistry componentRegistry,
			final PageDocument pageDocument, final Element element) {
		super(componentRegistry, pageDocument, element);
	}

	public EToolbar getToolbar() {
		return toolbar == null ? EToolbar.Basic : toolbar;
	}

	public void setToolbar(final EToolbar toolbar) {
		this.toolbar = toolbar;
	}

	public String getTextarea() {
		return textarea;
	}

	public void setTextarea(final String textarea) {
		this.textarea = textarea;
	}

	public boolean isToolbarCanCollapse() {
		return toolbarCanCollapse;
	}

	public void setToolbarCanCollapse(final boolean toolbarCanCollapse) {
		this.toolbarCanCollapse = toolbarCanCollapse;
	}

	public boolean isResizeEnabled() {
		return resizeEnabled;
	}

	public void setResizeEnabled(final boolean resizeEnabled) {
		this.resizeEnabled = resizeEnabled;
	}

	public boolean isStartupFocus() {
		return startupFocus;
	}

	public void setStartupFocus(final boolean startupFocus) {
		this.startupFocus = startupFocus;
	}

	public ELineMode getEnterMode() {
		return enterMode == null ? ELineMode.p : enterMode;
	}

	public void setEnterMode(final ELineMode enterMode) {
		this.enterMode = enterMode;
	}

	public ELineMode getShiftEnterMode() {
		return shiftEnterMode == null ? ELineMode.br : shiftEnterMode;
	}

	public void setShiftEnterMode(final ELineMode shiftEnterMode) {
		this.shiftEnterMode = shiftEnterMode;
	}

	public String getHtmlContent() {
		return htmlContent;
	}

	public void setHtmlContent(final String htmlContent) {
		this.htmlContent = htmlContent;
	}

	public String getJsLoadedCallback() {
		return jsLoadedCallback;
	}

	public void setJsLoadedCallback(final String jsLoadedCallback) {
		this.jsLoadedCallback = jsLoadedCallback;
	}

	@Override
	protected String[] elementAttributes() {
		return new String[] { "htmlContent", "jsLoadedCallback" };
	}
}
