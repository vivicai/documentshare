package net.simpleframework.my.space;

import net.simpleframework.organization.IJob;
import net.simpleframework.organization.IUser;
import net.simpleframework.organization.account.IAccount;
import net.simpleframework.util.LocaleI18n;
import net.simpleframework.web.AbstractTitleAwarePageLoad;
import net.simpleframework.web.page.PageParameter;

public class MySpacePageLoad extends AbstractTitleAwarePageLoad {

	@Override
	public Object getBeanProperty(final PageParameter pageParameter, final String beanProperty) {
		if ("title".equals(beanProperty)) {
			final IAccount account = MySpaceUtils.getAccountAware().getSpecifiedAccount(pageParameter);
			String title = null;
			if (account != null) {
				final IUser user = account.user();
				if (user != null) {
					title = user.getText() + LocaleI18n.getMessage("MySpacePageLoad.0");
				}
			}
			title = wrapApplicationTitle(title);
			if (title != null) {
				return title;
			}
		} else if ("jobView".equals(beanProperty)) {
		} else if ("importPage".equals(beanProperty)) {
			if ("space".equals(pageParameter.getRequestParameter("t"))) {
				String[] pages;
				if (MySpaceUtils.getAccountAware().isMyAccount(pageParameter)) {
					pages = new String[] { MySpaceUtils.deployPath + "jsp/space_log.xml", MySpaceUtils.deployPath + "jsp/space_log_editor.xml" };
				} else {
					pages = new String[] { MySpaceUtils.deployPath + "jsp/space_log.xml" };
				}
				return addImportPage(pageParameter, pages);
			} else if ("corpus".equals(pageParameter.getRequestParameter("t"))) {
				String[] pages = new String[] { "/simple/docu/corpus/mycorpus_list.xml" };
				return addImportPage(pageParameter, pages);
			} else {
				String[] pages = new String[] { "/simple/docu/my/mydocu.xml" };
				return addImportPage(pageParameter, pages);
			}
		}
		return super.getBeanProperty(pageParameter, beanProperty);
	}
}
