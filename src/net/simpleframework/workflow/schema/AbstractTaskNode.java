package net.simpleframework.workflow.schema;

import java.util.ArrayList;
import java.util.Collection;

import org.dom4j.Element;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public abstract class AbstractTaskNode extends Node {

	private Collection<String> listeners;

	public AbstractTaskNode(final Element dom4jElement, final ProcessNode processNode) {
		super(dom4jElement, processNode);
	}

	public Collection<TransitionNode> fromTransitions() {
		final ArrayList<TransitionNode> al = new ArrayList<TransitionNode>();
		for (final Node node : ((ProcessNode) getParent()).nodes()) {
			if (node instanceof TransitionNode) {
				final TransitionNode transition = (TransitionNode) node;
				if (getId().equals(transition.getTo())) {
					al.add(transition);
				}
			}
		}
		return al;
	}

	public Collection<TransitionNode> toTransitions() {
		final ArrayList<TransitionNode> al = new ArrayList<TransitionNode>();
		for (final Node node : ((ProcessNode) getParent()).nodes()) {
			if (node instanceof TransitionNode) {
				final TransitionNode transition = (TransitionNode) node;
				if (getId().equals(transition.getFrom())) {
					al.add(transition);
				}
			}
		}
		return al;
	}

	public Collection<String> listeners() {
		return listeners;
	}

	@Override
	public void syncElement() {
		super.syncElement();
	}
}
