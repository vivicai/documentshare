package net.simpleframework.workflow.schema;

import org.dom4j.Element;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class StartNode extends AbstractTaskNode {

	public StartNode(final Element element, final ProcessNode processNode) {
		super(element == null ? addNode(processNode, "start-node") : element, processNode);
	}
}
