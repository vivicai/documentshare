package net.simpleframework.workflow.schema;

import org.dom4j.Element;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class AbstractParticipantType extends AbstractNode {
	private String participant;

	public AbstractParticipantType(final Element dom4jElement, final AbstractNode parent) {
		super(dom4jElement, parent);
	}

	public String getParticipant() {
		return participant;
	}

	public void setParticipant(final String participant) {
		this.participant = participant;
	}

	static Element addParticipant(final AbstractNode parent, final String name) {
		return addElement(parent, "participant-type", name);
	}
}
